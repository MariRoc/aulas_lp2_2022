
<nav class="navbar navbar-expand-lg navbar-light bg-light">

  <div class="container-fluid">

    <button
      class="navbar-toggler"
      type="button"
      data-mdb-toggle="collapse"
      data-mdb-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <i class="fas fa-bars"></i>
    </button>


    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      <a class="navbar-brand mt-2 mt-lg-0" href="<?= base_url('home') ?>">
        <h2>Controle Financeiro</h2>
      </a>

      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">Cadastro</a>
          <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="<?= base_url('usuario/cadastro') ?>">Usuário</a>
              <a class="dropdown-item" href="#">Conta Bancária</a>
              <a class="dropdown-item" href="#">Parceiros</a>
         </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">Lançamentos</a>
          <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">Contas a Pagar</a>
              <a class="dropdown-item" href="#">Contas a Receber</a>
              <a class="dropdown-item" href="#">Fluxo de Caixa</a>
         </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">Relatórios</a>
          <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">Lançamentos por Período</a>
              <a class="dropdown-item" href="#">Resumo Mensal</a>
              <a class="dropdown-item" href="#">Resumo Anual</a>
         </div>
        </li>
      </ul>

    </div>

  </div>
 
</nav>>