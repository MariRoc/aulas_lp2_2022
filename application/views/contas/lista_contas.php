<div class="container">
    <div class="row mt-5">
        <div class="col"> 
            <?= $lista ?>
        </div>
    </div>

    <div class="mt-5 mb-5">
        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample"
            role="button" aria-expanded="false" aria-controls="collapseExample">
            Nova Conta
        </a>
    </div>
    <div class="collapse mt-3" id="collapseExample">
        <div class="row">
            <div class="col-md-6 mx-auto border mt-5 pt-3 pb-2">
                <form method="POST" id="contas_form">
                    <input class="form-control" name= "parceiro" type="text" placeholder=" Devedor / Credor "> <br>
                    <input class="form-control" name= "descricao" type="text" placeholder=" Descrição "> <br>
                    <input class="form-control" name= "valor" type="number" placeholder=" Valor"> <br>

                    <div class="row">
                        <div class="col-md-6">
                            <input class="form-control" name="mes" type="number" placeholder="Mês"> 
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" name="ano" type="number" placeholder="Ano"> 
                        </div>
                    </div>
                    <input type="hidden" name="tipo" value="<?= $tipo ?>"><br><br>
                    <div class="text-center text-md-left">
                        <a class="btn btn-primary" onclick="document.getElementById('contas_form').submit();">Enviar</a>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>