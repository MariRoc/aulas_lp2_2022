<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller{
    public function index(){
        $this->load->view('common/cabecalho');
        $this->load->view('common/navbar');
        $this->load->view('common/rodape');
    }
}